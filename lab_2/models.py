from django.db import models

class Note(models.Model):

    To = models.CharField(max_length=30, default='')
    From = models.CharField(max_length=30, default='')
    Title  = models.CharField(max_length=30, default='')
    Message =models.TextField(default='')