from django.shortcuts import render
from django.http import HttpResponse
from .models import Note
from django.core import serializers

# Create your views here.
def index(request):
    Notes = Note.objects.all()
    response = {'Notes': Notes}
    return render(request, 'lab2.html', response)

def xml(request):
    # load note = ?
    Notes = Note
    data = serializers.serialize('xml', Notes.objects.all())
    return HttpResponse(data, content_type="application/xml") 

def json(request):
    Notes = Note
    data = serializers.serialize('json', Notes.objects.all())
    return HttpResponse(data, content_type="application/json")
