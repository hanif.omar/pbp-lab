from django.db import models
from lab_2.models import Note
import datetime
import uuid
# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

def f():
    d = uuid.uuid4()
    str = d.hex
    return str[0:6].upper()

class Friend(models.Model):
    name = models.CharField(max_length=30, default='')
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=30, default='')
    DOB = models.DateField(default=datetime.date.today)
    # quizId = uuid.uuid4().hex[:6].upper()(unique = True)
    quizId = models.CharField(max_length=6, default=f, unique=True)
    mail = models.ForeignKey(Note, default=None,on_delete=models.CASCADE)

