# Generated by Django 3.2.7 on 2021-11-03 17:40

from django.db import migrations, models
import lab_1.models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0004_alter_friend_mail'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='quizId',
            field=models.CharField(default=lab_1.models.f, max_length=6, unique=True),
        ),
    ]
