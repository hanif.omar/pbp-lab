from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
# Create your views here.
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            return redirect('/lab-3')
        

    context['form']= form
    return render(request, "lab3_form.html", context)