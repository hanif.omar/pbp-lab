from django.db import models

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=30, default='')
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=30, default='')
    DOB = models.DateField()
    def __str__(self):
        return self.title